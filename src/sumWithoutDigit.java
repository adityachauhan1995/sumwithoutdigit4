import java.math.BigInteger;
import java.util.Scanner;

public class sumWithoutDigit {
	public static void main(String[] args) {
		
		System.out.println("Enter number :");
		Scanner myObj = new Scanner(System.in);
		String temp = myObj.next();
		BigInteger number = new BigInteger(temp);
		BigInteger bigIntTwo = new BigInteger("2");
		
		if(number.mod(bigIntTwo).equals(BigInteger.ZERO)) {
			BigInteger evenHalf = number.divide(bigIntTwo);
			String evenHalfString1 = evenHalf.toString();
			evenHalfString1 = evenHalfString1.replace('4','3');
			String evenHalfString2 = evenHalf.toString();
			evenHalfString2 = evenHalfString2.replace('4','5');
			System.out.println("First  no: " + evenHalfString1);
			System.out.println("Second no: " + evenHalfString2);
			
		}else {
			BigInteger oddhalf1 = number.divide(bigIntTwo);
			String oddHalfString1 = oddhalf1.toString();
			BigInteger oddhalf2 = oddhalf1.add(BigInteger.ONE);
			String oddHalfString2 = oddhalf2.toString();
			
			if(oddHalfString1.charAt(oddHalfString1.length()-1) == '3' ) {
				oddhalf1 = oddhalf1.subtract(BigInteger.ONE);
				oddHalfString1 = oddhalf1.toString();
				oddhalf2 = oddhalf2.add(BigInteger.ONE);
				oddHalfString2 = oddhalf2.toString();
			}
			else if(oddHalfString1.charAt(oddHalfString1.length()-1) == '4') {
				oddhalf1 = oddhalf1.add(bigIntTwo);
				oddHalfString1 = oddhalf1.toString();
				oddhalf2 = oddhalf2.subtract(bigIntTwo);
				oddHalfString2 = oddhalf2.toString(); 
			}
			oddHalfString1= oddHalfString1.replace('4','3');
			oddHalfString2 = oddHalfString2.replace('4','5');
			System.out.println("First  no: " + oddHalfString1);
			System.out.println("Second no: " + oddHalfString2);
		}
	}
}
